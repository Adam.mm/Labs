import java.util.Scanner;

//  Author: Adam Morrin Date: September 2016
//	a.	Write a program in Java that reads in the name of a person in two Strings, a first name and a last name
//	b.	Determine and store the length of each string 
//	c.	Determine and store the initials of the person
//	d.	Determine and store the last character of each name
//	e.	Determine and store whether the first name contains the character 'a'
//	f.	Display the name in all upper case
//	g.	Display the length of the Strings
//	h.	Display the initials of the person
//	i.	Display the last characters of the first and last name
//	j.	Display the index position of 'a'  in the first name

public class reverseName
{
	public static void main (String args[])
	{
		int		index = 0;
		int		firstNameLength;
		int 	lastNameLength;
		int		letterAOcc = 0;
		int		letterAPos = 0;	
		char	firstNameIni;
		char	lastNameIni;
		char	firstNameLast;
		char	lastNameLast;
		
		Scanner sc = new Scanner(System.in);
		 
		System.out.print("Please enter your first name : ");
		String firstName = sc.next();			
		System.out.print("Please enter your last name : ");
		String lastName = sc.next();
		
		//B. Determining and outputing the length of the first and last name
		firstNameLength = firstName.length();
		lastNameLength = lastName.length();
		System.out.print("\nThe length of your first name is : " + firstNameLength);
		System.out.print("\nThe length of your last name is : " + lastNameLength);
		
		//C. Determining the first letters of each name and outputing them
		firstNameIni = firstName.charAt(0);
		lastNameIni = lastName.charAt(0);
		
		//D. Determining the last letters of each name and outputing them
		firstNameLast = firstName.charAt(firstNameLength -1);
		lastNameLast = lastName.charAt(lastNameLength -1);
		
		//E. Determining and storing whether the first name contains the character 'a'
		for(index = 0 ; index < firstName.length() ; index ++ )
			{
				if(firstName.charAt(index) == 'a' || firstName.charAt(index) == 'A')
					{
						letterAOcc ++;
						letterAPos = index;
					}
			}	
		System.out.print("\nThe letter 'a' was found " + letterAOcc + " amount of times.");
		
		//F Display the name in all upper case
		System.out.print("\nYour name in Uppercase: " + firstName.toUpperCase() + lastName.toUpperCase());
		
		//G Display the length of the Strings
		System.out.print("\nFirst Name length: " + firstNameLength + "\nLast Name length: " + lastNameLength);
		
		//H Display the initials of the person
		System.out.print("\nThe intials are : " + firstNameIni + lastNameIni);
		
		//I Display the last characters of the first and last name
		System.out.print("\nThe last letters of your name are : " + firstNameLast + lastNameLast);
		
		//J Display the index position of 'a' in the first name
		System.out.print("\nThe postion of 'a' in your first name is index : " + (letterAPos + 1));
		
		
		System.out.print("\nNAME: " + firstName + " "  + lastName);


		}
}