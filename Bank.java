package ie.lab10a;
//Adam Morrin
//C00202212
import java.util.Scanner;

public class Bank {

	public static void main(String[] args) 
	{
		
		SavingsAccount Acc1 = new SavingsAccount();//firstObject

		Scanner sc = new Scanner(System.in);//Initializing the scanner class
		
		System.out.print("\nPlease enter the amount of money in account " + Acc1.getAccountNumber() + " : ");
		int savingsBal = sc.nextInt(); 
		Acc1.setSavingsBalance(savingsBal);
		
		
		System.out.print(Acc1.toString());
		//prints what was already set in toString method
		sc.close();
	}
	

}
