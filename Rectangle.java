package ie.lab9b;
//Author : Adam Morrin
//Date : 27/10/2016
//Desc : Develop a java class called Rectangle. 
//The class has attributes length and width, each of which defaults to 1 in the constructor. 
//It has set and get methods for both length and width. 
//The set methods should verify that length and width are each numbers larger than 0.0 and less than or equal to 40.0.
//Lastly, the class should have a toString() method which will return a string like the following:"Length = 5,  Width = 10�
//Write a suitable driver program to test each of your methods in class Rectangle.
//Extend your Rectangle class in Q1 by adding two new methods getArea() and getPerimeter() 
//that calculate the area and perimeter of the rectangle respectively. 
//Test these by calling the new methods from your driver program.
//Extend your Rectangle class in Q1 by adding a new method printRectangle() which will draw the rectangle 
//object by printing �*� to delineate the edges.
//e.g. if you create a rectangle object with width = 5 and length = 7 and call the printRectangle() method you should get the following output:
//*****
//*   *
//*   *
//*   *
//*   *
//*   *
//*****
//Similarly, an object with width = 10 and length = 4, should output:
//**********
//*        *
//*        *
//**********


public class Rectangle {
	private double length;
	private double width;
	private double area;
	private double perimeter;
	private String shape;
	
	
	//Initializing the variables to 1
	public Rectangle()
	{
		length = 1;
		width = 1;
		area = 1;
		perimeter = 1;
		shape = "*";
	}
	
	public String printRectangle()
	{
		int index = 0;
		int index2 = 0;//second index for the making the walls of the rectangle
		
		for(index = 0; index < width - 1; index++) //loops once index is less than width -1
		{
			shape = shape + "*"; //when it loops it adds a * to the shape, creating the top of the rectangle
		}
		
		shape = shape + "\n"; //goes to the next line
		index = 0; //Reinitialize index to 0 as it has been incremented in the last loop
		while(index < length -2) //loop while index is less than length -2 , this is so it does not add another two onto the users input
		{
			for(index2 = 0; index2 <= width; index2 ++) //loop until index2 is less than or equal to the width
			{
				if(index2 == 0 || index2 == width - 1) // if the index2 is equal to 0 or is equal to the width , this ensures the wall is created on the very start or end of the width of the rectangle
				{
					shape = shape + "*"; //adding the *
				}
				else
				{
					shape = shape + " "; //adding a space when the index2 isnt creating a wall, gives the space in the middle of the square
				}
				if(index2 == width) // this skips to the next line when one is done
				{
					shape = shape + "\n";
				}
			}
			index++;
		}
		index = 0;
		for(index = 0; index < width; index++) //loops once index is less than width -1
		{
			shape = shape + "*"; //when it loops it adds a * to the shape, creating the bottom of the rectangle
		}
		return shape;
	}
	//constructor method for length
	public void reclength()
	{
		setLength(0);
	}
	//GETTER FOR RECTANGLE LENGTH\\
	public double getLength() 
	{
		return length;
	}
	//SETTER FOR RECTANGLE LENGTH\\
	public void setLength(double length) 
	{
		  if (length < 0.0 || length >= 40.0)  //if statement ensures the length is between 0 and 40 otherwise the length is set to 0
		  {
			  length = 0.0;
		  } 
		  else 
		  {
			  this.length = length;
		  }
	}
	
	//constructor method for width
	public void recwidth()
	{
		setWidth(0);
	}
	//GETTER FOR RECTANGLE WIDTH\\
	public double getWidth() 
	{
		return width;
	}
	//GETTER FOR RECTANGLE WIDTH\\
	public void setWidth(double width) 
	{
		  if (width < 0.0 || width >= 40.0) //if statement ensures the width is between 0 and 40 otherwise the width is set to 0
		  {
			  width = 0.0;
		  } 
		  else 
		  {
			  this.width = width;
		  }
	}  
	//constructor method for area
	public void recarea()
	{
		setArea(0);
	}
	//GETTER FOR AREA\\
	public double getArea() 
	{
		return length * width;//returns the length multiplied by width
	}		
	//SETTER FOR AREA\\
	public void setArea(double area) 
	{
		this.area = area;
	}
	
	//constructor method for perimeter
	public void recperimeter()
	{
		setPerimeter(0);
	}
	//GETTER FOR PERIMETER\\
	public double getPerimeter() 
	{
		return 2 * (length + width); //returns the length + width multiplied 2
	}
			
	//SETTER FOR PERIMETER\\
	public void setPerimeter(double perimeter) 
	{
		this.perimeter = perimeter;
	}
	
	public String toString()
			{	
			String text;
			text = "Length: "+ getLength() + "\nWidth: " + getWidth() + "\nArea: " + getArea() + "\nPerimeter: " + getPerimeter() + "\n";
			return text;
			}
			
	}
	
	

