package ie.lab6;
import java.util.Scanner;

//	Author: Adam Morrin Date: October 2016
//	Write a Java application to input a single integer 
//	value and output the corresponding number of *'s

public class lab6q2 {

	public static void main(String[] args)
	{
		char star = '*';
		int index = 0;
		
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("\nPlease enter the amount of stars you want: \nEnter -1 to exit");
		int starAmount = sc.nextInt();
		
		for(index = 0; index < starAmount; index++)
		{
			if(starAmount == -1)
			{
				index = starAmount;
			}
			else
			{
				System.out.print(star);
			}
			
			
		}
		
		System.out.print("\nGoodbye!!");

	}

}
