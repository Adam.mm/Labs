import java.util.*;
import java.io.*;
import java.util.Scanner;

/*
** Author: Adam Morrin Date: September 2016
** Takes several inputs and outputs results
*/
public class numbers
{
	public static void main (String args[])
	{
		 Scanner sc = new Scanner(System.in);
		 
		System.out.print("Enter any Integer : ");
		int num1 = sc.nextInt();
		System.out.print("Enter any Float : ");
		float float1 = sc.nextFloat();
		System.out.print("Enter any Long : ");
		long long1 = sc.nextLong();
		System.out.print("Enter any Double : ");
		double double1 = sc.nextDouble();
		System.out.print("Enter any String : ");
		String rand = sc.next();
		
		System.out.print("---INPUTS--");
		System.out.print("\nInteger : " + num1);
		System.out.print("\nFloat : " + float1);
		System.out.print("\nLong : " + long1);
		System.out.print("\nDouble : " + double1);
		System.out.print("\nString : " + rand);
		System.out.print("---ADDITIONS--");
		System.out.print("\nInteger + Float : " + (num1 + float1));
		System.out.print("\nInteger + Long: " + (num1 + long1));
		System.out.print("\nInteger + Double: " + (num1 + double1));
		System.out.print("\nInteger + String: " + (num1 + rand));
		System.out.print("\nFloat + Long : " + (float1 + long1));
		System.out.print("\nFloat + Double : " + (float1 + double1));
		System.out.print("\nFloat + String : " + (float1 + rand));
		System.out.print("\nLong + Double: " + (long1 + double1));
		System.out.print("\nLong + String : " + (long1 + rand));
		System.out.print("\nDouble + String : " + (double1 + rand));
		}
}