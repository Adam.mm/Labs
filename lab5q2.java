import java.util.Scanner;

//  Author: Adam Morrin Date: September 2016

// Description: Program to compute the powers of 2 from 1 to 10
// Demonstrates the use of the while control structure
//
// Pseudocode:
//
// Initialise the current power of 2 to 1
// While the current power of 2 is <= 10
//    Calculate the current power of 2
//    Print the current power of 2
//    Add 1 to the current power of 2
// End while


public class lab5q2
{
   public static void main(String args[])
   {
      int power = 1, result;
      final int MAX = 10;

      while ( power <= MAX )
      {
         result = (int) Math.pow(11,power);
         System.out.println( "11 to the power of " + power + "= " + result);
         power++;
      }
   }
}
