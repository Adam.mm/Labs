import java.util.*;
import java.io.*;
import java.util.Scanner;

/*
** Author: Adam Morrin Date: September 2016
** Takes several inputs and outputs results
*/
public class reverseName
{
	public static void main (String args[])
	{
		int		index = 0;
		int		firstNameLength;
		int		lastNameLength;
		Scanner sc = new Scanner(System.in);
		 
		System.out.print("Please enter your first name : ");
		String firstName = sc.next();			
		System.out.print("Please enter your last name : ");
		String lastName = sc.next();
		
		//Determining and outputing the length of the first and last name
		firstNameLength = firstName.length();
		lastNameLength = lastName.length();
		System.out.print("\nThe length of your first name is : " + firstNameLength);
		System.out.print("\nThe length of your last name is : " + lastNameLength);
		
		//Determining the first letters of each name and outputing them
		System.out.print("\nThe intials are : " + firstName.charAt(0) + lastName.charAt(0));
		
		//Determining the last letters of each name and outputing them
		System.out.print("\nThe last letters of each name are : " + firstName.charAt(firstNameLength -1) + " " + lastName.charAt(lastNameLength -1) );
		
		for(index = firstName.length() -1; index >= 0; index--)
			{
				if(firstName.charAt("a"))
				{
					System.out.print("\nYour first name does contain the letter 'a'");
				}
				else
				{
					System.out.print("\nYour first name does not contain the letter 'a'");
				}
			}	

		System.out.print("\nNAME: " + firstName + " "  + lastName);


		}
}