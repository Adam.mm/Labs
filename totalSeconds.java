import java.util.Scanner;

//  Author: Adam Morrin Date: September 2016

//	Create a program to read values representing a 
//	time duration in hours, minutes, and seconds, 
//	then print the equivalent total number of seconds.

public class totalSeconds
{
	public static void main (String args[])
	{
		int totalSeconds;
		
		
		Scanner sc = new Scanner(System.in);
		
		
		System.out.print("\nPlease enter the amount of Hours: ");
		int hours = sc.nextInt();
		System.out.print("\nPlease enter the amount of Minutes: ");
		int minutes = sc.nextInt();
		System.out.print("\nPlease enter the amount of Seconds: ");
		int seconds = sc.nextInt();
		
		totalSeconds = (hours * 60 * 60) + (minutes * 60)  + seconds;
		
		System.out.print("\nTotal amount of Seconds: " + totalSeconds);		
		System.out.print("\n");	

	}
}