package ie.lab10a;
//Adam Morrin
//C00202212
//Create a class SavingsAccount. 
//Each SavingsAccount should have a unique number that is automatically assigned by the constructor method,
//i.e. the number is not to be passed as a parameter to the constructor. 
//The account numbers should start at 1 and count upwards in increments of 1.
//Use a static class variable to store the annualInterestRate for each of the savers.
//Each object of the class contains a private instance variable savingsBalance indicating the amount 
//the saver currently has on deposit.

public class SavingsAccount {
	
	final double annualInterestRate;  
	private double savingsBalance;
	private int accountNumber; 
	
	public SavingsAccount()
	{
		annualInterestRate = ;
		savingsBalance = 0;
		accountNumber = 0;
	}
	

	
	//constructor method for accountNumber
	public void accountNumber()
	{
		setAccountNumber(0);
	}
	//getter method for accountNumber
	public int getAccountNumber() 
	{
		return accountNumber;
	}
	//setter method for accountNumber
	public void setAccountNumber(int accountNumber)
	{
		this.accountNumber = accountNumber + 1;
	}

	
	//constructor method for savingsBalance
	public void savingsBalance()
	{
		setSavingsBalance(0);
	}
	//getter method for savingsBalance
	public double getSavingsBalance() 
	{
		return savingsBalance;
	}
	//setter method for savingsBalance
	public void setSavingsBalance(double savingsBalance)
	{
		this.savingsBalance = savingsBalance;
	}

	//constructor method for annualInterestRate
	public void annualInterestRate()
	{
		setSavingsBalance(0);
	}
	//getter method for annualInterestRate
	public double getAnnualIntRate() 
	{
		return annualInterestRate;
	}
	//setter method for annualInterestRate
	public void setAnnualIntRate(double intRate)
	{
		this.annualInterestRate = ;
	}







	public String toString()
	{	
	String text;
	text = "********************************\n"  + "Account Number: "+ getAccountNumber() + "Interest Rate: " + getAnnualIntRate() + "\n" + "Savings Account Balcance: " + getSavingsBalance() ;
	return text;
	}
}
