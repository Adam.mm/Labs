
// Student Name : 		Adam Morrin
// Student Id Number : 	C00202212
// Date :				22/09/2016
// Purpose : 			Prints the message hello world to the screen

public class Hello
{
	//prints a simple message...
	public static void main (String args[])
	{
		System.out.print("Hello , I'm a Java Program\n");
	}
}