package ie.lab9b;
import java.util.Scanner;

public class Shapes {

	public static void main(String[] args) 
	{
		// Create an instance of our HotelRoom class
		Rectangle rec1 = new Rectangle();//firstObject
		//printRectangle shape1 = new printRectangle();
		
		Scanner sc = new Scanner(System.in);//Initializing the scanner class
		
		System.out.print("\nSet the length the Rectangle: ");
		int recLengthTempL = sc.nextInt(); 
		rec1.setLength(recLengthTempL);//setting rec length

		System.out.print("\nSet the width the Rectangle: ");
		int recLengthTempW = sc.nextInt(); 
		rec1.setWidth(recLengthTempW);//setting rec width
		
		
		
		System.out.print(rec1.toString());//prints what was already set in toString method
		System.out.print(rec1.printRectangle());
		sc.close();
	}
	

}
