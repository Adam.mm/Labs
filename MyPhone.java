//Author	: Oisin Cawley
//Date		: March-2016
//Purpose	: Displays a frame demonstrating the use of panels and flow managers.
package ie.lab14;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JTextArea;

// Driver class
public class MyPhone 
{	
	public static void main(String[] args) 
	{
	    Frame f = new PhoneFrame("My Phone");
	    f.setSize(300, 250);
	    f.setVisible(true);
	}
}

// Frame class
class PhoneFrame extends Frame 
{
	private TextField display = new TextField(20);
	
	
	public PhoneFrame(String title) 
	{
		
	    super(title);
	    
		Panel displayPanelTop = new Panel();
	    JTextArea  result = new JTextArea();
		displayPanelTop.add(result);
		
		Panel displayPanel = new Panel();
		displayPanel.add(display);
		//display.setEditable(false);
		add("North", displayPanel);

		Panel buttonPanel = new Panel();
		buttonPanel.setLayout(new GridLayout(4, 3, 10, 10));

	    // Create button listener
	    ButtonListener listener = new ButtonListener();
	    
	    Button b;
		for (int i = 1; i <= 9; i++)
		{
			b = new Button(i + "");
			b.addActionListener(listener);
			buttonPanel.add(b);
		}
	
		Panel centerPanel = new Panel();
		centerPanel.add(buttonPanel);
		add("Center", centerPanel);
	
		Panel bottomPanel = new Panel();
		bottomPanel.add(new Button("+"));
		bottomPanel.add(new Button("="));
		bottomPanel.add(new Button("-"));
		add("South", bottomPanel);
	

	    // Attach window listener
	    addWindowListener(new WindowCloser());

	}

	// Listener for all buttons
	class ButtonListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent evt) 
		{
	  		String buttonLabel = evt.getActionCommand();

	  		// Test label on button and update display as appropriate
			display.setText(display.getText() + buttonLabel);
		}
	}
	
	// Listener for window
	class WindowCloser extends WindowAdapter 
	{
		public void windowClosing(WindowEvent evt) 
		{
	    	System.exit(0);
	  	}
	}
	
}


