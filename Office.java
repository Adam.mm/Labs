package ie.lab11;


public class Office {
	
	private String officeName;
	private String officeType;
	private int roomNumber;
	
	
	
	public Office()
	{
		officeName = "";
		officeType = "";
		roomNumber = 100;
		
		
	}

	//OFFIC NAME
	public void officeName()
	{
		setOfficeName("");
	}
	
	
	public String getOfficeName() {
		return officeName;
	}


	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}


	//OFFICE TYPE
	public void officeType()
	{
		setOfficeType("");
	}
	
	public String getOfficeType() {
		return officeType;
	}

	public void setOfficeType(String officeType) {
		this.officeType = officeType;
	}

	//OFFICE/ROOM NUMBER
	public void roomNumber()
	{
		setRoomNumber(100);
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber++;
	}
	
	public String toString()
	{
		String text;
		text = "Office Name: " + getOfficeName() + "\nOffice Type: " + getOfficeType() + "\nRoom Number: " + getRoomNumber() + "\n";
		return text;
	}

}
