package ie.lab11;


public class Address {
	
	private String street;
	private String citytown;
	private String county;
	
	
	
	public Address()
	{
		street = "";
		citytown = "";
		county = "";	
	}

	
	//STREET
	public void streetName()
	{
		setStreet("");
	}
	
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}


	//CITY OR TOWN
	public void cityTownName()
	{
		setCitytown("");
	}
	
	public String getCitytown() {
		return citytown;
	}

	public void setCitytown(String citytown) {
		this.citytown = citytown;
	}

	
	//COUNTY
	public void countyName()
	{
		setCounty("");
	}
	
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	public String toString(){
		String text;
		text = "Street: " + getStreet() + "\nCity/Town: " + getCitytown() + "\nCounty: " + getCounty() + "\n";
		return text;
	}
}