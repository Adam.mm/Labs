import java.util.Scanner;

//  Author: Adam Morrin Date: September 2016

//	Write a program that reads a value representing a number of seconds. 
//	Print the equivalent amount of time in hours, minutes, and seconds.

public class valueOfSeconds
{
	public static void main (String args[])
	{
		double hours;
		int minutes;
		
		Scanner sc = new Scanner(System.in);
		
		
		System.out.print("\nPlease enter the amount of Seconds: ");
		int seconds = sc.nextInt();

	//	seconds = (hours / 60 / 60) + (minutes / 60)  + seconds;
		hours = (seconds / 60 / 60);
		minutes = (seconds / 60);
		seconds = seconds;
		
		System.out.print("\nTotal amount of Hours: " + hours);	
		System.out.print("\nTotal amount of Minutes: " + minutes);	
		System.out.print("\nTotal amount of Seconds: " + seconds);	
			
		System.out.print("\n");	

	}
}