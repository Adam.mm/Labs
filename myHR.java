package ie.lab11;
import java.util.Scanner;
//Author : Adam Morrin
//Date : 11/1/2017

public class myHR {

	public static void main(String[] args) 
	{

		// Create an instance of our Employee class
		Employee emp1 = new Employee();//firstObject
		Office off1 = new Office();//firstObject
		Address add1 = new Address();
		
		Scanner sc = new Scanner(System.in);//Initializing the scanner class
		
		System.out.print("\nEmployees First Name: ");
		String firstName = sc.nextLine(); 
		emp1.setFirstName(firstName);
		
		System.out.print("\nEmployees Last Name: ");
		String lastName = sc.nextLine(); 
		emp1.setLastName(lastName);
		
		System.out.print("\nEmployees Position (Staff / Manager): ");
		String empPosition = sc.nextLine(); 
		emp1.setPosition(empPosition);
		
		if(emp1.getPosition() == "Manager" || emp1.getPosition() == "manager")
			{
				System.out.print("\nCompany Car Type: ");
				String carType = sc.nextLine(); 
				emp1.setCarType(carType);
				
				System.out.print("\nCompany Car Cost: ");
				int carCost = sc.nextInt(); 
				emp1.setCarCost(carCost);
				
				System.out.print("\nCompany Car Fuel Type: ");
				String fuelType = sc.nextLine(); 
				emp1.setFuelType(fuelType);
			}
		
		System.out.print("\nEmployees Age: ");
		int age = sc.nextInt();
		emp1.setAge(age);		
		
		System.out.print("\nOffice Name: ");
		String officeName = sc.next(); 
		off1.setOfficeName(officeName);
		
		System.out.print("\nOffice Type: ");
		String officeType = sc.next(); 
		off1.setOfficeType(officeType);
		
		System.out.print("\nStreet: ");
		String street = sc.next();
		add1.setStreet(street);
		
		System.out.print("\nCity/Town: ");
		String citytown = sc.next();
		add1.setCitytown(citytown);
		
		System.out.print("\nCounty: ");
		String county = sc.next();
		add1.setCounty(county);


		
		
		System.out.print("\n" + emp1.toString());
		System.out.print("\n" + off1.toString());
		System.out.print("\n" + add1.toString());
		
		sc.close();
	}
}
