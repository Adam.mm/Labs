package ie.lab9;
import java.util.Scanner;

public class RoomCheckIn {

	public static void main(String[] args) 
	{
		//Initializes two roomvac variables to used to the find the room vacancy 
		String roomAVac = "";
		String roomBVac = "";
		
		// Create an instance of our HotelRoom class
		HotelRoom roomA = new HotelRoom();//firstObject
		HotelRoom roomB = new HotelRoom();//secondObject
		
		Scanner sc = new Scanner(System.in);//Initializing the scanner class
		
		System.out.print("\nSet the room number of Room: ");
		int TempRoomNum1 = sc.nextInt(); 
		roomA.setroomNumber(TempRoomNum1);//setting roomNumber for roomA
		System.out.print("\nPlease enter the type of room (Single or Double): ");
		String TempRoomType1 = sc.next();
		roomA.setroomType(TempRoomType1);//setting roomType for roomA
		System.out.print("\nSet the vacancy of Room: ");
		int vancancy1 = sc.nextInt(); 
		roomA.setvacancy(vancancy1);//setting vacancy for roomA
		System.out.print("\nSet the rate of Room: ");
		Double rate2 = sc.nextDouble(); //setting rate for roomA
		roomA.setrate(rate2);
		
		//if statements that get the values of vacancy entered by the user and if equaled to 0 vacant and if 1 occupied
		if(roomA.getvacancy() == 0 || roomA.getvacancy() == 1 )
		{
			if(roomA.getvacancy() == 1)//if user input is equal to 1, room is set to occupied
			{
				roomAVac = "Occupied";
			}
			else if(roomA.getvacancy() == 0)// if user input is equal to 0, room is set to vacant
			{
				roomAVac = "Vacant";
			}
		}
		else if(roomA.getvacancy() > 1)
		{
			roomAVac = "The wrong info was enter. Please try again.";
		}
		
		System.out.print("\nSet the room number of Room: ");
		int TempRoomNum2 = sc.nextInt(); 
		roomB.setroomNumber(TempRoomNum2);
		System.out.print("\nPlease enter the type of room (Single or Double): ");
		String TempRoomType2 = sc.next();
		roomB.setroomType(TempRoomType2);
		System.out.print("\nSet the vacancy of Room: ");
		int vancancy2 = sc.nextInt(); 
		roomB.setvacancy(vancancy2);
		System.out.print("\nSet the rate of Room: ");
		Double rate1 = sc.nextDouble(); 
		roomB.setrate(rate1);
		
		if(roomB.getvacancy() == 0 || roomB.getvacancy() == 1 )
		{
			if(roomB.getvacancy() == 1)
			{
				roomBVac = "Occupied";
			}
			else if(roomB.getvacancy() == 0)
			{
				roomBVac = "Vacant";
			}
		}
		else if(roomB.getvacancy() > 1)
		{
			roomBVac = "The wrong info was enter. Please try again.";
		}
		
		
		System.out.print("\nRoom A Number: " + roomA.getroomNumber());
		System.out.print("\nRoom A Type: " + roomA.getroomType());	
		System.out.print("\nRoom A Vacancy: " + roomAVac);	
		System.out.print("\nRoom A Rate: " + roomA.getrate());	
		System.out.print("\nRoom B Number: " + roomB.getroomNumber());
		System.out.print("\nRoom B Type: " + roomB.getroomType());	
		System.out.print("\nRoom B Vacancy: " + roomBVac);	
		System.out.print("\nRoom B Rate: " + roomB.getrate());	
		
		sc.close();
	}
	

}
