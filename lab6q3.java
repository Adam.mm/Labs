package ie.lab6;
import java.util.Scanner;

//	Author: Adam Morrin Date: October 2016
//	Write a Java application to input a single integer 
//	value and output the corresponding number of *'s

public class lab6q3 {

	public static void main(String[] args)
	{
		char star = '*';
		int index = 0;
		int index2;
		
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("\nPlease enter the amount of stars you want: ");
		int starAmount = sc.nextInt();
		
		for(index = 0; index <= starAmount; index ++)
		{
			System.out.print(star);
			System.out.print("");
			starAmount = starAmount - 1;
		}
		
	}

}
