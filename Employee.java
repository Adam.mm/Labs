package ie.lab11;

public class Employee {
	
	private String firstName;
	private String lastName;
	private int age;
	private String position;
	private int employeeNumber;
	private int empAmount;
	private String carType;
	private int carCost;
	private String fuelType;

	//Initializing the variables to 1
	public Employee()
	{
		firstName = "";
		lastName = "";
		age = 0;
		position = "Unknown";
		employeeNumber = 1000;
		empAmount = 0;
		carType = "";
		carCost = 0;
		fuelType = "";
		
	}
	
	
	//FIRST NAME
	public void firstname()
	{
		setFirstName("");
	}
	
	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	
	}
	
	
	//LAST NAME
	public void lastname()
	{
		setLastName("");
	}
	
	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	//position
	public void position()
	{
		setPosition("Unknown");
	}
	
	
	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}
 
	
	//AGE
	public void age()
	{
		setAge(0);
	}
	

	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}
	
	//EMPLOYEE NUMBER
	public void employeeNumber()
	{
		setEmployeeNumber(1000);
	}
	
	public void incrementEmpNumber(){
		employeeNumber++;
	}


	public int getEmployeeNumber() {
		return employeeNumber;
	}


	public void setEmployeeNumber(int empNum) {
		this.employeeNumber = empNum++;
	}
	
	//AMOUNT OF EMPLOYEES
	public void amount()
	{
		setEmpAmount(0);
	}
	
	public void incrementEmp(){
		empAmount++;
	}

	public int getEmpAmount() {
		return empAmount;
	}

	public void setEmpAmount(int amount) {
		this.empAmount = amount++;
	}

	
	//CAR TYPE
	public void carType()
	{
		setCarType("");
	}
	
	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	//CAR COST
	public void carCost()
	{
		setCarCost(0);
	}
	public int getCarCost() {
		return carCost;
	}
	
	public void setCarCost(int carCost) {
		this.carCost = carCost;
	}

	//CAR FUEL TYPE
	public void fuelType()
	{
		setFuelType("");
	}
	
	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}


	
	
	//TO STING PRINT
	public String toString()
			{	
			String text;
			text = "Name: "+ getFirstName() + " " + getLastName() + "\nPosition: " + getPosition() + "\nAge: " + getAge() + "\nEmployee Number: " + getEmployeeNumber()  + "\nAmount of Employees: " + getEmpAmount() + "\n";
			if(getPosition() == "Manager" || getPosition() == "manager")
				{
					text += "\nCompany Car Type: " + getCarType() + "\nCompany Car Cost: " + getCarCost() + "\nCompany Car Fuel Type: " + getFuelType() + "\n";
				}
			return text;
			}
			

}
