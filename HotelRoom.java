package ie.lab9;
/*Develop a java class called HotelRoom which will be used to manage the rooms in a Hotel. 
 * The class should contain a private integer value called roomNumber, and a private string 
 * called roomType (which can be either �Single� or �Double�). Write the getter and setter 
 * methods for each of these variables. You will also need to write the necessary constructor 
 * method which takes no arguments.
Write a driver program which instantiates 2 room objects from this class as follows:
roomA (room number is 200, type is �Single�)
roomB (room number is 201, type is �Double�).
The program should then print out the details for these rooms by calling the appropriate getter methods.
*/
public class HotelRoom {

	private int roomNumber;
	private String roomType;
	private int vacancy;
	private double rate;
	//private double rate;
	
	public HotelRoom()
	{
		roomNumber = 0;
		roomType = "";
		vacancy = 0; //0 means vacant , 1 means occupied
		rate = 0;
	}
	/*ROOM NUMBERS*/	
	public void roomNumber()
	{
		setroomNumber(0);
	}
	
	/*setter for roomNumber*/
	public void setroomNumber(int rNum)
	{
		 roomNumber = rNum;		
	}
	/*getter for roomNumber*/
	public int getroomNumber()
	{
		return roomNumber;
	}
	
	
	/*ROOM TYPE*/
	public void roomType()
	{
		setroomType("Single");
	}
	/*setter for roomType*/
	public void setroomType(String rType)
	{
		roomType = rType ;		
	}
	/*getter for roomType*/
	public String getroomType()
	{
		return roomType;
	}
	
	
	/*ROOM VANCANCY*/
	public void vacancy()
	{
		setvacancy(0);
	}
	/*setter for vacancy*/
	public void setvacancy(int occu)
	{
		vacancy = occu ;		
	}
	/*getter for vacancy*/
	public int getvacancy()
	{
		return vacancy;
	}
	
	
	/*ROOM RATE*/
	public void rate()
	{
		setrate(0);
	}
	/*setter for rate*/
	public void setrate(double rateAmount)
	{
		rate = rateAmount ;		
	}
	/*getter for rate*/
	public double getrate()
	{
		return rate;
	}
}
