import java.util.Scanner;

//  Author: Adam Morrin Date: September 2016

//	BigBurgers Ltd require a program to calculate their profit on hamburgers and chips each day. 
//	A portion of chips cost the company 30p and sells at 75p. 
//	A hamburger costs 40p and sells at 95p. 
//	Follow the usual design code and test steps to develop a program as follows:
//	Input to the program is the number of hamburgers and the number of portions of chips sold.
//	Output is the profit on hamburgers, the profit on chips and the total profit.

public class bigBurgers
{
	public static void main (String args[])
	{
		//declaring finals and variables
		final double BURGERPRICE = 0.95;
		final double CHIPSPRICE = 0.75;
		final double BURGERCOST = 0.40;
		final double CHIPSCOST = 0.30;
	
		double burgerTotal;
		double chipsTotal;
		double totalProfit;
		
	
		Scanner sc = new Scanner(System.in);
		
		//input SOPS
		System.out.print("\nPlease enter amount of Burgers sold: ");
		int  burgerAmount = sc.nextInt();
		System.out.print("\nPlease enter amount of Chips sold: ");
		int  chipsAmount = sc.nextInt();
		
		//sums
		burgerTotal = (burgerAmount * BURGERPRICE) - (burgerAmount * BURGERCOST);
		chipsTotal = (chipsAmount * CHIPSPRICE) - (chipsAmount * CHIPSCOST);		
		totalProfit = (burgerTotal + chipsTotal);
		
		//output SOPS
		System.out.print("\nProfit on Burgers Sold: " + burgerTotal);
		System.out.print("\nProfit on Chips Sold: " + chipsTotal);
		System.out.print("\nTotal Profit: " + totalProfit);
		

	}
}