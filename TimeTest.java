package ie.lab10a;
//Driver program to test the Time class.
public class TimeTest
{
	public static void main (String args[])
	{
		Time times = new Time (23, 59, 59);
		System.out.println(times.toMilitaryString());
		System.out.println(times.toString());
		times.tick();
		System.out.println(times.toMilitaryString());
		System.out.println(times.toString());
	}
}